/// CSE 02 Homework 2

public class Homework2{
  
  public static void main(String args[]){
    //Calculates total cost per item, slaes tax per item, total cost before tax, total sales tax, and total for transaction.
    
//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
    
//Calculates total cost for Pants
    double TotalPantsCost = (numPants * pantsPrice);
   System.out.println("Cost of Pants = $" + TotalPantsCost);
    
//Calculates total cost for sweatshirts
    double TotalSweatshirtCost = (numShirts * shirtPrice);
   System.out.println("Cost of Sweatshirts = $" + TotalSweatshirtCost);

//Calculates total cost for belts
    double TotalBeltCost = (numBelts * beltCost);
    System.out.println("Cost of Belt = $" + TotalBeltCost);
    
//Calculates sales tax for pants 
    double SalesTaxPants = (TotalPantsCost * paSalesTax);
    SalesTaxPants = ((int)(SalesTaxPants * 100))/100.0;
    System.out.println("Sales Tax for Pants = $" + SalesTaxPants);
    
//Calculates sales tax for sweatshirts
    double SalesTaxSweatshirts = (TotalSweatshirtCost * paSalesTax);
    SalesTaxSweatshirts = ((int)(SalesTaxSweatshirts * 100))/100.0;
    System.out.println("Sales Tax for Sweatshirts = $" + SalesTaxSweatshirts); 
    
//Calculates sales tax for belts 
    double SalesTaxBelt = (TotalBeltCost * paSalesTax);
    SalesTaxBelt = ((int)(SalesTaxBelt * 100))/100.0;
    System.out.println("Sales Tax for Belt = $" + SalesTaxBelt);
    
//Calculates Total Cost Before Sales Tax
    double TotalCost = (TotalPantsCost + TotalSweatshirtCost + TotalBeltCost);
    System.out.println("Total Cost Before Tax = $" + TotalCost);
    
//Calculates Total Sales Tax 
    double TotalSalesTax = (SalesTaxPants + SalesTaxSweatshirts + SalesTaxBelt);
    System.out.println("Total Sales Tax = $" + TotalSalesTax);
    
//Calculates Total Cost of Transaction
    double OverallCost = (TotalCost + TotalSalesTax);
    System.out.println("Overall Cost = $" + OverallCost);
  }
}
