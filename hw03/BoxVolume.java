//import Scanner class
import java.util.Scanner; 
public class BoxVolume{
  
  public static void main(String[] args){
   // Takes inputs of width, height, and length of a box and outputs the volume 
    //Declares x as the object of the scanner
    Scanner x = new Scanner(System.in);
    
    //Establishes width, length, height, and volume to be doubles
    double width, length, height, volume; 
    
    //Print statment to enter width of the box
    System.out.println("Enter width of the box: ");
    //Allows width to be an input
    width = x.nextDouble();
    
    //Print statement to enter length of the box
    System.out.println("Enter length of the box: ");
    //Allows length to be an input
    length = x.nextDouble();
    
    //Print statement to enter height of the box
    System.out.println("Enter height of the box: ");
    //Allows height to be an input
    height = x.nextDouble();
    
    //Formula for volume of a box
    volume = (width * length * height);
    
    //Print statement for final answer
    System.out.println("Volume of Box is: " + volume );
}
}