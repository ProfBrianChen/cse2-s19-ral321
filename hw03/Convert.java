//import Scanner class
import java.util.Scanner;
public class Convert{
  
  public static void main(String[] args){
    //Takes input of meters and outputs the equivalent amount of inches
    
    //final variable and creates conversion
    final double Inches = 39.3701;
    
    //Declares x as the object of the Scanner
    Scanner x = new Scanner(System.in);
    
    //Prints statement to input meters
    System.out.println("Enter the distance in meters: ");
    
    //Establishes metes as double and allows meters to be an input
    double meter = x.nextDouble();
    
    //Establishes inches as a double and outputs conversion formula
    double inches = meter * Inches;
    
    //Print statement to show input of (X) meters is equal to (Y) inches
    System.out.println(meter + " meters is " + inches + " inches");
    
  }
}