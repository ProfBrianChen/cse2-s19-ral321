import java.util.Scanner;
public class Area{
  public static void main(String[] args){
   
    String s1 = new String ("triangle");
    String s2 = new String ("rectangle");
    String s3 = new String ("circle");
    
    //Prompts user to enter triangle, rectangle, or circle. Checks if input is correct. 
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter triangle, rectangle, or circle. ");
    String input = scan.nextLine();
    while(!input.equals(s1) && !input.equals(s2) && !input.equals(s3)){
        System.out.println("Im sorry, incorrect input, try again lad!");
        System.out.println("Enter triangle, rectangle, or circle. ");
        input = scan.nextLine();
    }
        //Decides which method to use based on input.
    if(input.equals(s1)){
      triangleArea();
    }
    else if (input.equals(s2)){
      rectangleArea();
    }
    else{
      circleArea();
    }
    
   }
  
  //Method for triangle
   public static void triangleArea(){
      double base = 0.0;
      double height = 0.0;
      while(base == 0.0 && height == 0.0){
        System.out.println("Enter the base:");
        base = getDouble();
        System.out.println("Enter the Height:");
        height = getDouble();
      }
     
      double area = (base * height) / 2.0;
     
      System.out.println("Area: " + area);
        
   }
  
  //Method for rectangle 
    public static void rectangleArea(){
      double length = 0.0;
      double width = 0.0; 
      while(length == 0.0 && width == 0.0){
        System.out.println("Enter the length: ");
        length = getDouble();
        System.out.println("Enter the width: ");
        width = getDouble();
      }
        double area = (length * width);
        System.out.println("Area: " + area);
    }
    
    //Method for circle
    public static void circleArea(){
      double pi = 3.14;
      double radius = 0.0; 
      while(pi == 3.14 && radius == 0.0){
        System.out.println("Enter radius: ");
        radius = getDouble();
      }
        double area = pi * (radius * radius);
        System.out.println("Area: " + area);
    }
    
    //Method to ensure user entered a double
    public static double getDouble(){
       Scanner myScanner = new Scanner(System.in);
       while(!myScanner.hasNextDouble()){
         System.out.println("Try again. Enter a double.");
         myScanner.next();
       }
      double input = myScanner.nextDouble();
       
      
      return input;

    }
  
}

