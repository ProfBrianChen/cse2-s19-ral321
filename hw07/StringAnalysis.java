import java.util.Scanner;
public class StringAnalysis{
  public static void main(String[] args){
    
    //Enter String
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter String: ");
    String input = scan.nextLine();
    
    //Enter the number of characters to be checked.
    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter the number of characters you want to check.");
    int i = 0;
    if(scanner.hasNext()){
    i = scanner.nextInt();
    }
    //Decide which method to use.
    boolean allLetters = false;
    if(i != 0){
        allLetters = letters(input, i);
    }else{
        allLetters = letters(input);
    }
    
    System.out.println("The String is all letters: " + allLetters);
    
  }
  
  //Method for if i = 0
  public static boolean letters(String input){
    if(input.matches(".*\\d.*")){
      return false;
    }else{
     return true;
    }
  }
  
  //Method for if i > 0
  public static boolean letters(String input, int numLetters){
    boolean result = true;  
    
    if(input.length() < numLetters){
      numLetters = input.length();
    }
    
    for(int i=0; i < numLetters; i++){
      if(!Character.isLetter(input.charAt(i))){
          return false;
      }
    } 
      
    
    
    return result;
         
  }
  
  
}