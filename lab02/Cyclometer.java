//Rayn Lynn CSE002 Lab02
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {

 int secsTrip1=480;  // Seconds for first trip
 int secsTrip2=3220;  //Seconds for second trip
 int countsTrip1=1561;  //Counts for first trip
 int countsTrip2=9037; //Counts for second trip
      
 double wheelDiameter=27.0,  //diameter of wheel
  	PI=3.14159, //value for pi
  	feetPerMile=5280,  //number of feet in a mile
  	inchesPerFoot=12,   //number of inches in 1 foot
  	secondsPerMinute=60;  //number of seconds in 1 minute 
	double distanceTrip1, distanceTrip2,totalDistance;  //converts distances in trips 1 and 2 and total distanse into a double 

  System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+
   countsTrip1+" counts."); //Print statment for Trip 1
	 System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+
     countsTrip2+" counts."); //Print statment for Trip 2 
      
  distanceTrip1=countsTrip1*wheelDiameter*PI;
   // Above gives distance in inches
   //(for each count, a rotation of the wheel travels
   //the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;

  //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
 
     
      
      
	}  //end of main method   
} //end of class
