
import java.util.Random;
public class CardGenerator{
  public static void main (String[] args){
      
    int numValue = (int)(Math.random() * 13) +1;
    
    
    String value = "";
    
    switch(numValue){
        
      case 1: 
        value = "Ace"; 
        break;
      case 2: 
        value = "2"; 
        break; 
      case 3: 
        value = "3"; 
        break; 
      case 4: 
        value = "4"; 
        break; 
      case 5:
        value = "5";
        break; 
      case 6: 
        value = "6";
        break; 
      case 7:
        value = "7";
        break; 
      case 8:
        value = "8";
        break; 
      case 9:
        value = "9";
        break; 
      case 10:
        value = "10";
        break; 
      case 11:
        value = "Jack";
        break; 
      case 12:
        value = "Queen"; 
        break;
      case 13:
        value = "King";
        break;
      default:
        value = "----ERROR----";
        break;
    }
    
   
   int numSuit = (int)(Math.random() * 4) +1;
    
   String suit = "";
    
    switch (numSuit){
    case 1:
      suit = "Hearts";
      break; 
    case 2:
      suit = "Spades";
      break;
     case 3:
       suit = "Clubs";
       break; 
     case 4:
       suit = "Diamonds"; 
       break; 
      default:
        suit = "--FIX RANDOM RANGE--";
       
    }
    
    System.out.println("Your card is the " + value + " of " + suit + ".\n");
  
  
  
   }
  
  }

  