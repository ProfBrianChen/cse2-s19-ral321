import java.util.Scanner; 
public class PatternB{
public static void main (String [] args){
    
 Scanner myScanner = new Scanner(System.in);
  int input;
  do{
    System.out.println("Enter an Integer Between 1-10: ");
    input = myScanner.nextInt();
    
    if (input < 1 || input > 10){
      System.out.println("Error: ");
      
    }
  }

  while (input < 1 || input > 10);
  
  System.out.println(" ");
  
  for (int i = input; i>=1; i--){
    for(int j = 1; j<=i; j++){
      System.out.print(j+ " ");
    }
    System.out.println(" ");
  }
}
}