import java.util.Scanner; 
public class PatternC{
public static void main (String [] args){
    
 Scanner myScanner = new Scanner(System.in);
  int input;
  do{
    System.out.println("Enter an Integer Between 1-10: ");
    input = myScanner.nextInt();
    
    if (input < 1 || input > 10){
      System.out.println("Error: ");
      
    }
  }

  while (input < 1 || input > 10);
  
  System.out.println(" ");
  
  for (int i = input; i <= input; i++){
  
    for(int j = 1; j >= 1; j--){
      System.out.print(j+ " ");
    }
    System.out.println();
  }
}
  }