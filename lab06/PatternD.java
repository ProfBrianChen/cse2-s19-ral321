import java.util.Scanner; 
public class PatternD{
public static void main (String [] args){
    
 Scanner myScanner = new Scanner(System.in);
  int input;
  do{
    System.out.println("Enter an Integer Between 1-10: ");
    input = myScanner.nextInt();
    
    if (input < 1 || input > 10){
      System.out.println("Error: ");
      
    }
  }

  while (input < 1 || input > 10);
  
  System.out.println(" ");
  
  for (int i = 1; i <= input; i++){
  
    for(int j = input + 1 - i; j > 0; j--){
      System.out.print(j + " " );
    }
    System.out.println();
  }
}
  }