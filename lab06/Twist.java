import java.util.Scanner;
public class Twist{
public static void main (String[] args){


//initialize Scanner and asks user for input
Scanner myScanner = new Scanner(System.in);
System.out.println("Length: ");
  
  //while loop that ensures user enters an integer if an integer is not input, loop will ask again
  while(!myScanner.hasNextInt()){
    System.out.println("Enter an Integer");
    System.out.println("Length: ");
    myScanner.next();
  }
  int userInput = myScanner.nextInt();

  
 
//for loop to print the first row of the twist   
 for(int i = 0; i  < userInput; i++){
//utilize modulus to alternate between 3 different characters to print. 
    if(i % 3 == 0){
      System.out.print("\\");
    }else if(i % 3 == 1){
      System.out.print(" ");
    }else if(i % 3 == 2){
      System.out.print("/");
    }else{
      System.out.println("Error");
    }
   
 }
  

System.out.println();
  //for loop to print the second row of the twist
  for(int j = 0; j  < userInput; j++){
    //utilize modulus to alternate between 3 different characters to print. 
    if(j % 3 == 0){
      System.out.print(" ");
    }else if(j % 3 == 1){
      System.out.print("X");
    }else if(j % 3 == 2){
      System.out.print(" ");
    }else{
      System.out.println("Error");
    }
   
 }

  System.out.println();
  //for loop to print the third row of the twist
  for(int k = 0; k  < userInput; k++){
    //utilize modulus to alternate between 3 different characters to print 
    if(k % 3 == 0){
      System.out.print("/");
    }else if(k % 3 == 1){
      System.out.print(" ");
    }else if(k % 3 == 2){
      System.out.print("\\");
    }else{
      System.out.println("Error");
    }
   
 }
  System.out.println();
  
  
  
  
}
}